﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using DemoEmpleadosParaTesting;
using NUnit.Framework;

namespace DemoEmpleadosParaTesting.UnitTest1
{
    [TestFixture]
    public class UnitTest1
    {
        [Test]
        public void SubirSueldo_Scenario_ExpectedBehavior()
        {
            var emp = new Empleado();
            emp.Antiguedad = 10;
            emp.Sueldo = 100;
            emp.SubirSueldo(20);

            Console.Write(emp.Sueldo);
                Assert.That(emp.Sueldo, Is.EqualTo(120));
        
            
        }
    }
}
